<?php

class Wp_Scss {
  /**
   * Compiling preferences properites
   *
   * @var string
   * @access public
   */
  public $scss_dir, $css_dir, $compile_method, $scssc, $compile_errors;


  /**
   * Set values for Wp_Scss::properties
   *
   * @param string scss_dir - path to source directory for scss files
   * @param string css_dir - path to output directory for css files
   * @param string method - type of compile (compressed, expanded, etc)
   *
   * @var object scssc - instantiate the compiling object.
   *
   * @var array compile_errors - catches errors from compile
   */
  public function __construct ($scss_dir, $css_dir, $compile_method) {
    $this->scss_dir = $scss_dir;
    $this->css_dir = $css_dir;
    $this->compile_method = $compile_method;
   
   // For now we will add these filters in the constructor
   
   // Provide additional scss directories
   $additional_scss_dirs = array();
   $additional_scss_dirs = apply_filters('wp_scss_scss_dirs', $additional_scss_dirs);
   
   $this->scss_dirs = array_merge(array($this->scss_dir), $additional_scss_dirs);
   
   // Provide additional raw scss
   $this->scss_raws = array();
   $this->scss_raws = apply_filters('wp_scss_scss_raws', $this->scss_raws);
   // Qualify the raws
   foreach ($this->scss_raws as $key => $raw) {
      if (!(
         isset($raw['raw'])
         && isset($raw['timestamp'])
         && isset($raw['handle'])
      )) {
         // Drop it
         unset($this->scss_raws[$key]);
      }
   }

    global $scssc;
    $scssc = new scssc();
    $scssc->setFormatter($compile_method);
    $scssc->setImportPaths($scss_dir);

    $this->compile_errors = array();
  }

 /**
   * METHOD COMPILE
   * Loops through scss directory and compilers files that end
   * with .scss and do not have '_' in front.
   *
   * @function compiler - passes input content through scssphp,
   *                      puts compiled css into cache file
   *
   * @var array input_files - array of .scss files with no '_' in front
   * @var array sdir_arr - an array of all the files in the scss directory
   *
   * @return nothing - Puts successfully compiled css into apporpriate location
   *                   Puts error in 'compile_errors' property
   * @access public
   */
  public function compile() {
    global $scssc, $cache;
    $cache = WPSCSS_PLUGIN_DIR . '/cache/';

    //Compiler - Takes scss $in and writes compiled css to $out file
    // catches errors and puts them the object's compiled_errors property
    function compiler($in, $outDir, $outFile, $instance) {
      global $scssc, $cache;

      if (is_writable($cache)) {
        try {
          $css = $scssc->compile($in);
          $outDir = str_replace("/", '-', $outDir);
          file_put_contents($cache.$outDir.'-'.$outFile, $css);
        } catch (Exception $e) {
          $errors = array (
            'file' => basename($in),
            'message' => $e->getMessage(),
          );
          array_push($instance->compile_errors, $errors);
        }
      } else {
        $errors = array (
          'file' => $cache,
          'message' => "File Permission Error, permission denied. Please make the cache directory writable."
        );
        array_push($instance->compile_errors, $errors);
      }
    }

    $inputs = array();
      
    // Loop through directory and get .scss file that do not start with '_'
    foreach ($this->scss_dirs as $scss_dir) {
      if (file_exists($scss_dir)) {
        foreach(new DirectoryIterator($scss_dir) as $file) {
          if (substr($file, 0, 1) != "_" && pathinfo($file->getFilename(), PATHINFO_EXTENSION) == 'scss') {
            array_push($inputs, array(
              'context' => 'file', 
              'file' => $file->getFileName(), 
              'dir' => $file->getPath(), 
              'pathname' => $file->getPathName(),
            ));
          }
        }
      } else {
        $errors = array (
          'file' => $scss_dir,
          'message' => "SCSS directory doesn't exist.  Please check the path."
        );
        array_push($instance->compile_errors, $errors);
     }
   }

   foreach ($this->scss_raws as $scss_raw) {
     array_push($inputs, array(
      'context' => 'raw', 
      'handle' => $scss_raw['handle'], 
      'raw' => $scss_raw['raw'],
     ));
   }

   // For each input, generate a matching css filename and compile
   foreach ($inputs as $scss) {
     $outputNames = array();
     switch ($scss['context']) {
       case 'file':
         $input = file_get_contents($scss['pathname']);
         $outputDir = str_replace(ABSPATH . "wp-content/", '', $scss['dir']);
         $outputFile = preg_replace("/\.[^$]*/",".css", $scss['file']);
         break;
       case 'raw':
         $input = $scss['raw'];
         $outputDir = "raw";
         $outputFile = "{$scss['handle']}.css";
         break;
     }

     compiler($input, $outputDir, $outputFile, $this);
   }

   if (count($this->compile_errors) < 1) {
     if  ( is_writable($this->css_dir) ) {
       foreach (new DirectoryIterator($cache) as $cache_file) {
         if ( pathinfo($cache_file->getFilename(), PATHINFO_EXTENSION) == 'css') {
           file_put_contents($this->css_dir.$cache_file, file_get_contents($cache.$cache_file));
           unlink($cache.$cache_file->getFilename()); // Delete file on successful write
         }
       }
     } else {
       $errors = array(
         'file' => 'CSS Directory',
         'message' => "File Permissions Error, permission denied. Please make your CSS directory writable."
       );
       array_push($this->compile_errors, $errors);
     }
   }
 }


  /**
   * METHOD NEEDS_COMPILING
   * Gets the most recently modified file in the scss directory
   * and compares that do the most recently modified css file.
   * If scss is greater, we assume that changes have been made
   * and compiling needs to occur to update css.
   *
   * @param string scss_dir - path to scss folder
   * @param string css_dir - path to css folder
   *
   * @var array sdir_arr - scss directory files
   * @var array cdir_arr - css directory files
   *
   * @var string latest_scss - file mod time of the most recent file change
   * @var string latest_css - file mod time of the most recent file change
   *
   * @return bool - true if compiling is needed
   */
  public function needs_compiling() {
    $latest_scss = 0;
    $latest_css = 0;

    foreach ($this->scss_dirs as $scss_dir) {
      if (file_exists($scss_dir)) {
        foreach ( new RecursiveIteratorIterator(new RecursiveDirectoryIterator($scss_dir)) as $sfile ) {
          if (pathinfo($sfile->getFilename(), PATHINFO_EXTENSION) == 'scss') {
            $file_time = $sfile->getMTime();
              if ( (int) $file_time > $latest_scss) {
                $latest_scss = $file_time;
              }
          }
        }
      } else {
        $errors = array (
          'file' => $scss_dir,
          'message' => "SCSS directory doesn't exist.  Please check the path."
        );
        array_push($instance->compile_errors, $errors);
      }
    }
     
    foreach ($this->scss_raws as $scss_raw) {
      if ((int) $scss_raw['timestamp'] > $latest_scss) {
        $latest_scss = $scss_raw['timestamp'];
      }
    }

    foreach ( new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->css_dir)) as $cfile ) {
      if (pathinfo($cfile->getFilename(), PATHINFO_EXTENSION) == 'css') {
        $file_time = $cfile->getMTime();

        if ( (int) $file_time > $latest_css) {
          $latest_css = $file_time;
        }
      }
    }     
    
    if ($latest_scss > $latest_css) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * METHOD ENQUEUE STYLES
   * Enqueues all styles in the css directory.
   *
   * @param $css_folder - directory from theme root. We need this passed in separately
   *                      so it can be used in a url, not path
   */
  public function enqueue_files($css_folder) {

    foreach( new DirectoryIterator($this->css_dir) as $stylesheet ) {
      if ( pathinfo($stylesheet->getFilename(), PATHINFO_EXTENSION) == 'css' ) {
        $name = $stylesheet->getBasename('.css') . '-style';
        $uri = get_stylesheet_directory_uri().$css_folder.$stylesheet->getFilename();
        $ver = $stylesheet->getMTime();


        wp_register_style(
          $name,
          $uri,
          array(),
          $ver,
          $media = 'all' );

        wp_enqueue_style( $name );
      }
    }
  }

} // End Wp_Scss Class
